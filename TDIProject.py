#!/usr/bin/env python
# coding: utf-8

# In[13]:


import pandas as pd
import numpy as np


# In[14]:


#Louisiana location is Little Rock, latitude is  34.91667
df_l = pd.read_csv('/home/david/Louisiana.csv', delimiter=',')


# In[15]:


for col in df_l.columns:
    print(col)


# In[16]:


df_l["DATE"] = pd.to_datetime(df_l["DATE"])


# In[17]:


#calculating solar insolation from cloud coverage data in 
#HourlySkyConditions
print(df_l["DATE"].count())
#First need to remove NaN, then parse out all info 
df_l = df_l[pd.isna(df_l["HourlySkyConditions"]) == False]
print(df_l["DATE"].count())


# In[18]:


# Cloud coverage data can 3 have 3 layers, all with 3 components
#Component 1 is a name, comp. 2 i okta, comp. 3 is cloud heigh (in 1000 ft)

#Splitting data into separate columns
df_test = df_l["HourlySkyConditions"].str.split(" ", n=6, expand = True)


# In[19]:


df_test0 = df_test[0].str.split(":", n=1, expand = True)
df_test2 = df_test[2].str.split(":", n=1, expand = True)
df_test4 = df_test[4].str.split(":", n=1, expand = True)


# In[20]:


cloud_data = { 'CLOUD LAYER 1 NAME' : df_test0[0],
            'CLOUD LAYER 1 OCTA' : df_test0[1],
            'CLOUD LAYER 1 HEIGHT' : df_test[1],
            'CLOUD LAYER 2 NAME' : df_test2[0],
            'CLOUD LAYER 2 OCTA' : df_test2[1],
            'CLOUD LAYER 2 HEIGHT' : df_test[3],
            'CLOUD LAYER 3 NAME' : df_test4[0],
            'CLOUD LAYER 3 OCTA' : df_test4[1],
            'CLOUD LAYER 3 HEIGHT' :  df_test[5] }


# In[21]:


df_cloud = pd.DataFrame(cloud_data)


# In[22]:


new_df = pd.concat([df_l, df_cloud], axis = 1)


# In[23]:


def solar_insolation_value (row):
    from numpy.core.umath import deg2rad
    louisiana_lat = deg2rad(34.91667)
    louisiana_long = -92.2892
    time_zone = -6
    adjusted_solar_noon = 12 + (((time_zone*15) - louisiana_long) / 15 ) 
    solar_constant = 1362
   # hour_angle = deg2rad(15) * (row["DATE"].hour + (row["DATE"].minute / 60.) - (12 + (12/60) + (57/3600)))
    hour_angle = deg2rad(15) * (row["DATE"].hour + (row["DATE"].minute / 60.) - adjusted_solar_noon)
    dec_angle = (deg2rad(-23.45) * np.cos(deg2rad(360) * (row["DATE"].day + 10) / 365))
    
    zenith_angle = np.arccos(np.sin(louisiana_lat)*np.sin(dec_angle) + 
                            np.cos(louisiana_lat)*np.cos(dec_angle)*np.cos(hour_angle))
    
    if np.cos(zenith_angle) < 0:
    #At night time, so solar insolation is negligible
        return 0
    else:
        return solar_constant * np.cos(zenith_angle)


# In[24]:


right_values = new_df.apply(lambda row : solar_insolation_value(row), axis = 1)
new_df["SOLAR INSOLATION"] = new_df.apply(lambda row: solar_insolation_value(row), axis = 1)


# In[25]:


#Sunrise time on Jan1 2013 : 17:17
print( new_df["DATE"].iloc[42], " ", new_df["SOLAR INSOLATION"].iloc[42])
print( new_df["DATE"].iloc[43], " ", new_df["SOLAR INSOLATION"].iloc[43])


# In[26]:


#Sunset time : 17:09
print( new_df["DATE"].iloc[63], " ", new_df["SOLAR INSOLATION"].iloc[63])
print( new_df["DATE"].iloc[64], " ", new_df["SOLAR INSOLATION"].iloc[64])


# In[27]:


#Looking at coverage fractions only as of now

octa_h20_dict = {0 : (235/235), 1 : (247/245), 2 : (230/235),
                 3 : (228/235), 4 : (199/235), 5 : (155/235),
                 6 : (152/235), 7 : (129/235), 8 : (62/235)}

octa_h30_dict = {0 : (354/354), 1 : (399/354), 2 : (301/354),
                 3 : (295/354), 4 : (287/354), 5 : (272/354),
                 6 : (233/354), 7 : (169/354), 8 : (109/354)}

octa_h40_dict = {0 : (531/531), 1 : (525/531), 2 : (502/531),
                 3 : (467/531), 4 : (423/531), 5 : (408/531),
                 6 : (359/531), 7 : (311/531), 8 : (178/531)}

octa_h50_dict = {0 : (668/668), 1 : (653/668), 2 : (623/668),
                 3 : (612/668), 4 : (546/668), 5 : (483/668),
                 6 : (425/668), 7 : (367/668), 8 : (196/668)}

octa_h60_dict = {0 : (751/751), 1 : (775/751), 2 : (725/751),
                 3 : (714/751), 4 : (590/751), 5 : (582/751),
                 6 : (574/751), 7 : (387/751), 8 : (219/751)}

octa_hplus_dict = {0 : (819/819), 1 : (825/819), 2 : (807/819),
                   3 : (767/819), 4 : (752/819), 5 : (624/819),
                   6 : (599/819), 7 : (475/819), 8 : (270/819)}

def determine_dict(zenith_angle):
    if zenith_angle < 20.:
        return octa_h20_dict
    elif zenith_angle < 30.:
        return octa_h30_dict
    elif zenith_angle < 40.:
        return octa_h40_dict
    elif zenith_angle < 50.:
        return octa_h50_dict
    elif zenith_angle < 60.:
        return octa_h60_dict
    else:
        return octa_hplus_dict


# In[28]:


import numpy as np


# In[29]:


years = new_df["DATE"].dt.year.unique()
months = new_df["DATE"].dt.month.unique()
days = new_df["DATE"].dt.day.unique()


# In[30]:


import re
def digitize(row, column):
    return re.sub("\D", "", str(row[column]))

new_df["CLOUD LAYER 1 OCTA"] = new_df.apply(lambda row: digitize(row, "CLOUD LAYER 1 OCTA"), axis = 1)
new_df["CLOUD LAYER 2 OCTA"] = new_df.apply(lambda row: digitize(row, "CLOUD LAYER 2 OCTA"), axis = 1)
new_df["CLOUD LAYER 3 OCTA"] = new_df.apply(lambda row: digitize(row, "CLOUD LAYER 3 OCTA"), axis = 1)


# In[113]:


yearly_averages = []
monthly_averages = []
daily_averages = []
monthly_cloudiness = []
cumulative_day = 0
for year in years:
    integrated_sol_ins_year = 0.0
    for month in months:
        integrated_sol_ins_month = 0.0
        cloud_hours_dict = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0,
                    5: 0, 6: 0, 7: 0, 8: 0}

        for day in days:            
            cumulative_day +=1
            cond_year = new_df["DATE"].dt.year == year
            cond_month = new_df["DATE"].dt.month == month
            cond_day = new_df["DATE"].dt.day == day
            
            df_temp = new_df[cond_year & cond_month & cond_day]
            
            integrated_sol_ins = 0.0
            day_measurements = 0
            cloud_hours_dict_day = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0,
                                5: 0, 6: 0, 7: 0, 8: 0}

            for row in range(1, df_temp["DATE"].count()):
                day_measurements +=1
                #Creating daily average graph
                octa_1 = 0
                octa_2 = 0
                octa_3 = 0
                del_t = df_temp["DATE"].iloc[row] - df_temp["DATE"].iloc[row-1]
                sol_ins_avg = (df_temp["SOLAR INSOLATION"].iloc[row] + 
                              df_temp["SOLAR INSOLATION"].iloc[row-1])/2.
                #Value in kWh
                if pd.isna(df_temp["CLOUD LAYER 3 OCTA"].iloc[row]) == False:
                    if df_temp["CLOUD LAYER 3 OCTA"].iloc[row] != '' :
                        octa_3 = int(df_temp["CLOUD LAYER 3 OCTA"].iloc[row])
                if pd.isna(df_temp["CLOUD LAYER 2 OCTA"].iloc[row]) == False:
                    if df_temp["CLOUD LAYER 2 OCTA"].iloc[row] != '' :    
                        octa_2 = int(df_temp["CLOUD LAYER 2 OCTA"].iloc[row])
                if pd.isna(df_temp["CLOUD LAYER 1 OCTA"].iloc[row] == False):
                    if df_temp["CLOUD LAYER 1 OCTA"].iloc[row] != '' :
                        octa_1 = int(df_temp["CLOUD LAYER 1 OCTA"].iloc[row])
    
                max_octa = max(octa_1, octa_2, octa_3)
                #VV and code 9 indicates obstructed sky - lookup codes avaialable to
                #find specific reason, but for now reduce to full coverage
                if max_octa > 8:
                    max_octa = 8
                    
                zenith_angle = np.arccos(df_temp["SOLAR INSOLATION"].iloc[row] / 1362)
                dict = determine_dict(zenith_angle)
                #solar insolation measured in kWh / m^2, so divide by 1000
                integrated_sol_ins += (del_t.seconds/3600.) * sol_ins_avg * dict[max_octa]
           
                #cloudiness hours heat map calc
                cloud_hours_dict_day.update({max_octa : cloud_hours_dict_day[max_octa] + 1})
            
            #dividing by total number of day measurements to normalize to 1
  #          print(day_measurements)
  #          print(cloud_hours_dict)
            for item in cloud_hours_dict:
                if day_measurements > 0.0:
                    cloud_hours_dict.update({item : cloud_hours_dict[item] + 
                                             (cloud_hours_dict_day[item] / day_measurements)})
                
            daily_averages.append([year, month, day, cumulative_day, integrated_sol_ins])
            integrated_sol_ins_month += integrated_sol_ins
            
        dict_list = []
        dict_list.append(year) 
        dict_list.append(month)
        for item in cloud_hours_dict:
            dict_list.append(cloud_hours_dict[item])
        monthly_cloudiness.append(dict_list)    
        monthly_averages.append([year, month, integrated_sol_ins_month])
        integrated_sol_ins_year += integrated_sol_ins_month
    yearly_averages.append([year, integrated_sol_ins_year])
            
            


# In[114]:


daily_df = pd.DataFrame(np.array(daily_averages),
                        columns = ["Year", "Month", "Day", 
                                   "Cumulative Day", "Solar Insolation kWh / m^2 / day"])

daily_df["Solar Insolation kWh / m^2 / day"] = daily_df["Solar Insolation kWh / m^2 / day"] / 1000.

monthly_df = pd.DataFrame(np.array(monthly_averages),
                          columns = ["Year", "Month", "Solar Insolation kWh / m^2 / day"])

monthly_df["Solar Insolation kWh / m^2 / day"] = monthly_df["Solar Insolation kWh / m^2 / day"] / (1000 * 30)

yearly_df = pd.DataFrame(np.array(yearly_averages),
                        columns = ["Year", "Solar Insolation kWh / m^2 / day"])
yearly_df["Solar Insolation kWh / m^2 / day"] = yearly_df["Solar Insolation kWh / m^2 / day"] / (1000 * 365)


# In[115]:


#Looking at how many days had no data for clouds
years = daily_df["Year"].unique()
months = daily_df["Month"].unique()
for year in years:
    for month in months:
        df_temp = daily_df[daily_df["Year"] == year]
        df_temp = df_temp[df_temp["Month"] == month]
        df_numzeroes = df_temp[df_temp["Solar Insolation kWh / m^2 / day"] == 0.0]
    
        print(year, " ", month, " ", df_numzeroes["Year"].count())


# In[116]:


import seaborn as sns
sns.set()


# In[117]:


daily_df.plot("Cumulative Day", "Solar Insolation kWh / m^2 / day", figsize=(20, 1))


# In[154]:


totals_list = []
for month in range(0, 12):
    new_list = []
    for octa in range(3, 11):
        new_list.append(monthly_cloudiness[month][octa] + monthly_cloudiness[month + 12][octa]
                        + monthly_cloudiness[month + 24][octa] + monthly_cloudiness[month + 36][octa]
                        + monthly_cloudiness[month + 48][octa] + monthly_cloudiness[month + 60][octa])
    totals_list.append(new_list)


# In[162]:


import matplotlib.pyplot as plt

ytick_list = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"]
xtick_list = ["1", "2", "3", "4", "5", "6", "7", "8"]
sns.heatmap(np.array(totals_list), xticklabels=xtick_list, yticklabels=ytick_list),

plt.ylabel("Month")
plt.xlabel("Octa")
plt.title("Number of Days Classified as Cloudy (excl. 0 Octas)")
plt.show()


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




